const functions = require('firebase-functions');
const cors = require('cors')({origin: true});

let QRCode, admin, https, fs, axios, request, snakeCaseKeys
let admin_initialized = false;


const modules = (req) => {
    switch (req){
        case 'admin':
            admin = require('firebase-admin')
            if(!admin_initialized){
            admin.initializeApp({
                storageBucket: 'swook-4f328.appspot.com',
                credential: admin.credential.cert(require("./swook-google-key.json"))
                })
                admin_initialized = true;
            }
            return admin
        case 'https': https = require('https'); return https;
        case 'fs': fs = require('fs'); return fs
        case 'axios': axios = require('axios'); return axios;
        default: return null
    }
}

exports.getInvoiceUrl = functions.region('europe-west3').https.onCall(async (data, context) => {
    const transaction = (admin || modules('admin')).firestore().collection('transactions').doc(data.transaction_id).get()
                                .then(res => res.data())
                                .catch(e => e)
    
    return transaction
})

exports.sendSupportMessage = functions.region('europe-west3').https.onCall(async (data, context) => {
    await (axios || modules('axios')).post('https://script.google.com/macros/s/AKfycbxvBDos1EhR89SjW1EMUu0xLDpyWW3YRAIlgRrljYt4AZpQjk3H/exec?q=a', {
        body: {
        recipent: 'john@avatarmetal.com',
        subject: 'Swook Support Message',
        body: `From: ${data.email} \n\nMessage: ${data.message}`
    }
    }, {
        headers: {
            'Content-Type': 'application/json',
            'contentType': 'application/json',
    }
})
    return {foo: 'bar'} 
})

exports.logError = functions.region('europe-west3').https.onCall(async (data, context) => {
    await (axios || modules('axios')).post('https://script.google.com/macros/s/AKfycbxvBDos1EhR89SjW1EMUu0xLDpyWW3YRAIlgRrljYt4AZpQjk3H/exec?q=a', {
        body: {
        recipent: 'john@avatarmetal.com',
        subject: 'Swook error',
        body: `Origin: ${data.origin} \n\nStack Trace: ${data.stack}`
    }
    }, {
        headers: {
            'Content-Type': 'application/json',
            'contentType': 'application/json',
    }
})
    return {foo: 'bar'}
})

exports.callBack = functions.region('europe-west3').https.onRequest((req, res) => {
    cors(req, res, async () => {
        try {
        console.log('did start write');
        const newDoc = await (admin || modules('admin')).firestore().collection('transactions').doc(req.body.id).set({
            status: req.body.status,
            payer_alias: req.body.payerAlias,
            error_code: req.body.errorCode,
            error_message: req.body.errorMessage,
        }, {merge: true})
        console.log('did succeed write, newDoc: ', newDoc);
        res.status(200).send('Successful write to DB')
    } catch (e) {
        console.log('fucked up: ', e);
        res.status(500).send('Error writing to DB')
    }
    })
})

exports.retrieveInvoice = functions.region('europe-west3').https.onRequest((req, res) => {
    cors(req, res, async () => {
        axios = (axios || modules('axios'))
        const userDocRef = (admin || modules('admin')).firestore().collection('users').doc(req.body.user_uid)
        const transactionDocRef = (admin || modules('admin')).firestore().collection('transactions').doc(req.body.payment_id)
        const user = await userDocRef.get().then((doc) => {
            return doc.data()
        }).catch((e) => e)
        const transaction = await transactionDocRef.get().then(doc => doc.data()).catch(e => e)
        
        const options = {
            headers: {
                'Access-Token': user.fortnox_data.access_token,
                'Client-Secret': functions.config().fortnox.client_secret,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
        }
        let customer;
        const customer_number = `Swook_${transaction.payer_alias}`
        const customer_exists = await axios.get(`https://api.fortnox.se/3/customers/${customer_number}`, options)
            .then(response => {customer = response.data; return response.data})
            .catch(e => {
                return false
            })
        if(!customer_exists){
            customer = await axios.post('https://api.fortnox.se/3/customers', {
                Customer: {
                    CustomerNumber: customer_number,
                    Name: transaction.payer_alias,
                    Type: 'PRIVATE',
                    Comments: 'Auto generated customer by Swook',
                }
            }, options)
                .then(newCustomer => newCustomer.data)
                .catch(e => e)
        }
        
        const invoice = await axios.post('https://api.fortnox.se/3/invoices', {
            Invoice: {
                CustomerNumber: customer_number,
                InvoiceType: 'CASHINVOICE',
                PaymentWay: 'CASH', // TODO: SETTING
                InvoiceRows: transaction.line_items.map(item => {
                    return {
                        ArticleNumber: item.ArticleNumber, 
                        DeliveredQuantity: item.quantity, 
                        Price: item.gross_price,
                    }
                }),
                Currency: 'SEK',
                VATIncluded: true,
                Remarks: `Swook order ${req.body.payment_id}`
            },
        }, options)
        .then(inv => inv.data.Invoice)
        .catch(e => e)
        
        const pdf = await axios({
            url: `${invoice['@url']}/print`,
            method: 'get',
            responseType: 'arraybuffer',
            headers: options.headers,
        })
            .then(response => response.data)
            .catch(e => e)
        
        const bucket = (admin || modules('admin')).storage().bucket()
        const destinationFile = bucket.file(`invoices/${req.body.payment_id}.pdf`)
        const invoice_ref = await destinationFile.save(pdf)
            .then(() => destinationFile.setMetadata({ contentType: 'application/pdf'}))
            .then(()=> destinationFile.getSignedUrl({
                action: 'read',
                expires: '03-09-2491'
            }))
            .then(signedUrls => signedUrls[0])
            .catch(e => {console.log(e)})
        
        // const qr_url = await (QRCode || modules('QRCode')).toDataURL(invoice_ref, { 
        //     width: 300, 
        //     errorCorrectionLevel: 'Low',
        //     color: {
        //         dark: '#b43093', 
        //         light: '#0000', 
        //       },
        // })
        axios({
            url: `${invoice['@url']}/bookkeep`,
            method: 'put',
            headers: options.headers,
        }) // SETTING eller vill man ha SMS ?? 

        transactionDocRef.set({
            fortnox_invoice_url: invoice['@url'],
            invoice_url: invoice_ref,
        }, {merge: true})
    res.status(200).send()  
    })
})

exports.authorizeFortnox = functions.region('europe-west3').https.onCall((data, context) => {
    try {
        const api_key = data.fortnox_data.api_key;
        const options = {
                            headers: {
                                'Authorization-Code': api_key,
                                'Client-secret': functions.config().fortnox.client_secret,
                            },
                        }
        const access_token =  async () => {
            const response = await (axios || modules('axios')).get('https://api.fortnox.se/3/articles/', options)
            if(response.status !== 200){throw new Error('Error requesting Auth token')}
            return response.data.Authorization.AccessToken; // cc7c819f-c08a-43bb-b3f9-385d94a2aa5c
        }   
        return access_token()
    } catch (e) {
        console.log('error', e)
        return e
    }
})


exports.revokeFortnox = functions.region('europe-west3').https.onCall(async (data, context) => {
    await (admin || modules('admin')).firestore().collection('users').doc(context.auth.uid).set({
        fortnox_data: {
            api_key: '',
            access_token: '',
            articles: []
        }
    }, {merge: true});
    if(data.fortnox_data.api_key === functions.config().fortnox.test_api_key){return true}
    try {
        const api_key = data.fortnox_data.api_key;
        const options = {
                            headers: {
                                'Authorization-Code': api_key,
                                'Client-secret': functions.config().fortnox.client_secret,
                            },
                        }
        return (axios || modules('axios')).get('https://api.fortnox.se/3/articles/', options)
    } catch (e) {
        console.log('error', e)
        return e
    }
})

exports.getArticlesFromFortnox = functions.region('europe-west3').https.onCall(async (data, context) => {
    let articles;
    try {
        const docRef = (admin || modules('admin')).firestore().collection('users').doc(context.auth.uid)
        const current_doc = await docRef.get().then((doc) => {
            return doc.data()
        }).catch((e) => e)
        const options = {
                            headers: {
                                'Access-Token': current_doc.fortnox_data.access_token,
                                'Client-Secret': functions.config().fortnox.client_secret,
                                'Content-Type': 'application/json',
                                'Accept': 'application/json'
                            },
                        }
        articles = await (axios || modules('axios')).get('https://api.fortnox.se/3/articles/', options)
        .then(response => {
            if(response.status !== 200){throw new Error('Error requesting articles')}
            return response.data.Articles;  
        })
        .catch(e => {console.log('could not fetch articles', e); return []});
        // merge articles
        const included = !current_doc.fortnox_data.articles.length
        articles.forEach((article, i) => {
            article.swook_data = { included }
            try {
                article.swook_data = current_doc.fortnox_data.articles.filter(oldArticle => oldArticle.ArticleNumber === article.ArticleNumber)[0].swook_data
            // eslint-disable-next-line no-empty
            } catch (e) {}
            // article.swook_data = (current_doc.fortnox_data.articles && current_doc.fortnox_data.articles[i] && current_doc.fortnox_data.articles[i].swook_data) || { include }
        })
        docRef.set({
            fortnox_data: {
                articles: articles, // what will happen here if error is thrown in then.catch above? will error message be logged in db?
            },
        }, {merge: true});
        return articles;
        // console.log('articles', articles)
    } catch (e) {
        console.log('error: ', e);
        return e
    }
})

exports.retrievePaymenttoken = functions.region('europe-west3').https.onCall((data, context) => {
        const url = 'https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests';
        const paymentReference = 'abc123'
        const payload = {
            payeePaymentReference : paymentReference,
            message: (`Din order till ${data.company_name}`).substring(0,49),
            callbackUrl: data.test_mode ? 'https://us-central1-swook-4f328.cloudfunctions.net/diesilent' : 'https://us-central1-swook-4f328.cloudfunctions.net/callBack',
            amount: data.amount,
            currency: 'SEK',
            payeeAlias: data.payeeAlias,
        };
        const httpsAgent = new (https || modules('https')).Agent({
            cert: (fs || modules('fs')).readFileSync("Swish_Merchant_TestCertificate_1234679304.pem", "ascii"),
            passphrase: "swish",
            ca: (fs || modules('fs')).readFileSync("Swish_TLS_RootCA.pem", "ascii"),
            key: (fs || modules('fs')).readFileSync("Swish_Merchant_TestCertificate_1234679304.key", "ascii")
        });
        const token = (axios || modules('axios')).post(url, payload, { httpsAgent })
            .then(response => {
            if(response.status !== 201){
                throw new Error('Swish did not respond with 201')
            }
            const payment_id = response.headers.location.replace('https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests/','')
            const docRef = (admin || modules('admin')).firestore().collection('transactions').doc(payment_id)
            docRef.set({
                created_at: new Date().toISOString(),
                paymentReference,
                payment_id,
                user_id: context.auth.uid,
                amount: data.amount,
                line_items: data.line_items, 
            })
            return {payment_token: response.headers.paymentrequesttoken, payment_id}
        })
        .catch(e => {
            console.log('error handler');
            throw new functions.https.HttpsError('cancelled', (e.response.data[0].errorMessage || e.message || 'Unhandled error'))
        })
        return token
  });


  exports.retrieveQr = functions.region('europe-west3').https.onRequest((req, res) => {
    cors(req, res, async () => {
    try {
        const body = req.body
        const qr = await (axios || modules('axios'))({
        url:  'https://mpc.getswish.net/qrg-swish/api/v1/commerce',
        method: 'post',
        responseType: 'arraybuffer',
        data: {
            token: body.payment_token,
            size: "300",
            format: "png",
            border: "0"
        }
        }).then(response => response.data)
        .catch(e => e)

        res.set('Content-Type', 'image/png').status(200).send(qr)
    } catch (e) {
        res.status(500).send(e.message)
    }
    });

  });

