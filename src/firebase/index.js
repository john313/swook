import store from '@/store'
import Firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/functions'

import App from '@/App.vue'
import vuetify from '@/plugins/vuetify'
import router from '@/routes'

const config = {
    apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
    authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
    // messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.VUE_APP_FIREBASE_APP_ID,
   };
   
let app; 

export default {
  install: (Vue) => {
    const firebase = Firebase.initializeApp(config)
    // const functions = Firebase.app().functions('europe-west3')
    // Firebase.app().functions('europe-west3');
    const db = firebase.firestore();
    if(location.hostname === 'localhost'){
      firebase.functions().useFunctionsEmulator("http://localhost:5001")
      db.settings({ 
        host: 'localhost:8081',
        ssl: false
      })
      Vue.prototype.$functions = firebase.functions() 
    } else {Vue.prototype.$functions = Firebase.app().functions('europe-west3')}
    const auth = firebase.auth()
    Vue.prototype.$auth = {
      login: async (username, pass) => {
        const user = await auth.signInWithEmailAndPassword(username, pass)
        if(user) store.dispatch('user/fetchUserData')
        return user
      },
      signInWithOauth: async payload => {
        const user = await store.dispatch('user/signUserUpWithOauth', payload)
        return user;
      },
      logout: async () => {
        await auth.signOut()
      }
    }
    auth.onAuthStateChanged(user => {
      store.commit('user/updateUser',{ user })
      if(!app){
        store.dispatch('user/fetchUserData')
        app = new Vue({
        vuetify,
        router,
        store,
        // functions,
        render: h => h(App)
        }).$mount('#app')
      }
    })
  }
}
