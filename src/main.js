import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import vuetify from 'vuetify'
// import App from './App.vue'
// import vuetify from '@/plugins/vuetify'
// import './plugins/vuetify'
import router from './routes'
import store from './store'
import FirebaseAuthPlugin from './firebase/'
import VueTelInputVuetify from 'vue-tel-input-vuetify/lib';



Vue.config.productionTip = false
Vue.use(VueTelInputVuetify, {
  vuetify,
});

sync(store, router)

Vue.use(FirebaseAuthPlugin)
// Vue.use(Vuetify)
export { router, store }
