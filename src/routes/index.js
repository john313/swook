import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import Dashboard from '@/views/Dashboard'
import Landing from '@/views/Landing'
import SignIn from '@/views/SignIn'
import SignUp from '@/views/SignUp'
import Profile from '@/views/Profile'
import Cart from '@/views/Cart'
import InvoiceDisplay from '@/views/InvoiceDisplay'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'landing',
      component: Landing,
      meta: {
        publicGuard: true,
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        authRequired: true,
        transitionName: 'slide',
      }
    },
    {
      path: '/signin',
      name: 'signin',
      component: SignIn,
      meta: {
        publicGuard: true,
        transitionName: 'slide',
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUp,
      meta: {
        publicGuard: true,
        transitionName: 'slide',
      }
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        authRequired: true,
        transitionName: 'slide',
      }
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
      meta: {
        authRequired: true,
        cartGuard: true,
        transitionName: 'slide',
      }
    },
    {
      path: '/invoices/:id',
      name: 'invoices',
      component: InvoiceDisplay,
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.authRequired) && !store.state.user.user) {
      next({ 
        path: '/signin',
        query: { redirect: to.fullPath }
      })
  } else if (to.matched.some(record => record.meta.publicGuard) && !!store.state.user.user) next({path: '/dashboard'})
  else if (to.matched.some(record => record.meta.cartGuard) && !store.state.transaction.cart.length) next({path: '/dashboard'})
   else next()
})

export default router
