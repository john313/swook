import Vue from 'vue'
import firebase from 'firebase'
import 'firebase/firestore';
import axios from 'axios'
import QRCode from 'qrcode'

const state = () => ({ 
    cart: [],
    swish: {
        qr_url: '',
        payment_token: '',
        payment_id: '',
        status: null,
        error_code: '',
        error_message: '',
        section: 'paying',
      },
})

const getters = {
    cart (state) {
        return state.cart
      },
      getArticle: (state, getters, rootState) => (article_number) => {
        return rootState.user.userProfile.fortnox_data.articles.find(article => article.ArticleNumber === article_number)
      },
      getCompiledCart (state) {
        let cart = []
        const items = state.cart
        items.forEach(item => {        
          if(cart.findIndex(a => a.ArticleNumber === item.ArticleNumber) !== -1) ++cart[cart.findIndex(a => a.ArticleNumber === item.ArticleNumber)].quantity
          else {item.quantity = 1; cart.push(item)}
        })
        return cart;
      },
}

const actions = {
  updateCart({state, commit, getters}, payload){
    const cart = state.cart
    const article = getters.getArticle(payload.article_number);
    if(payload.add === 1){
      cart.push(article)
      commit('setCart', cart)  
    } else {
      cart.splice(cart.findIndex(item => item.ArticleNumber === payload.article_number), 1)
      commit('setCart', cart)  
    }
  
  },
    async sendPaymentRequest({state, commit, rootState, getters, dispatch}){
        commit('app_logic/setLoading', true, {root: true})
        const amount = state.cart.map(item => item.gross_price).reduce((item, num) => {return num + item}, 0)
        const payeeAlias = rootState.user.userProfile.swish_data.payeeAlias
        const company_name = rootState.user.userProfile.company_name
        const paymentToken = this._vm.$functions.httpsCallable('retrievePaymenttoken')
        const test_mode = rootState.user.userProfile.fortnox_data.api_key === process.env.VUE_APP_TESTMODE_FORTNOX_KEY
        return paymentToken({ test_mode, amount, payeeAlias, company_name, line_items: getters.getCompiledCart })
          .then(res => {
            commit('setSwish', { payment_token: res.data.payment_token, payment_id: res.data.payment_id })
            return axios({
              method: 'POST',
              url: `${process.env.VUE_APP_SERVER_URL}retrieveQr`,
              responseType: 'blob',
              data: {
               payment_token: res.data.payment_token,
              }
             })
            }
          )
          .then(res => {
            const url = (window.URL || window.webkitURL).createObjectURL(res.data)
            commit('app_logic/setLoading', false, {root: true})
            commit('setSwish', { qr_url: url })
                
          })
          .catch(e =>  {
            commit('app_logic/setSnackBar', e.message, {root: true})
            commit('app_logic/setLoading', false, {root: true})
            dispatch('app_logic/logError', {
              error: e,
              origin: 'sendPaymentToken',
            }, {root: true})
          })
      },
      listenForCallback({commit, state, dispatch}){
        const unsubscribe_listener = firebase.firestore().collection("transactions").doc(state.swish.payment_id)
        .onSnapshot(function(doc) {
            const status = doc.data() && doc.data().status
            if(status){
              unsubscribe_listener()
              if(status === 'PAID'){
                commit('setSwish', doc.data())
                commit('app_logic/setLoading', true, {root: true})
                dispatch('retrieveInvoice', state.swish.payment_id)
                commit('resetCart')
              } else {
                commit('setSwish', {...doc.data(), section: 'error'})
              }
            }
        });
      },
      async retrieveInvoice({commit}, payload){
        // TODO: Lägg på error handling
        const qr_url = await QRCode.toDataURL(`${window.location.origin}/invoices/${payload}`, { 
          width: 300, 
          errorCorrectionLevel: 'Low',
          color: {
            dark: '#b43093', 
            light: '#0000', 
          },
        })
        commit('app_logic/setLoading', false, {root: true})
        commit('setSwish', {qr_url})
        axios({
          method: 'POST',
          url: `${process.env.VUE_APP_SERVER_URL}retrieveInvoice`,
          data: {
           payment_id: payload,
           user_uid: this.state.user.user.uid,
          }
         })

        //  .then(res => {
        //   commit('app_logic/setLoading', false, {root: true})
        //   commit('setSwish', {qr_url: res.data})
        //  })
        //  .catch(e => {
        //    commit('app_logic/setLoading', false, {root: true})
        //    dispatch('app_logic/logError', {
        //     error: e,
        //     origin: 'retrieveInvoice',
        //   }, {root: true})
        //    commit('setSwish', {section: 'error', error_message: `Betalningen har gått igenom, men något gick fel med att ta fram fakturan: ${e.message}`})
        //    commit('app_logic/setSnackBar', e.message, {root: true})
        //  })
      },
      addToCart({commit}, payload){
        commit('setCart', payload)
      },

}

const mutations = {
    setCart(state, payload) {
        Vue.set(state, 'cart', payload)
      },
      setSwish(state, payload) {
        Vue.set(state, 'swish', {
            ...state.swish,
            ...payload
          })
      },
      resetSwish (state){
        Vue.set(state, 'swish', {
          qr_url: '',
          payment_token: '',
          payment_id: '',
          status: null,
          error_code: '',
          error_message: '',
          section: 'paying',
        })
      },
      resetCart (state){
        Vue.set(state, 'cart', [])
      },
}


export default {
    state,
    getters,
    actions, 
    mutations,
    namespaced: true,
}

