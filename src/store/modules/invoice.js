import Vue from 'vue'

const state = () => ({ 
  token: '',
  url: '',
})

const actions = {
  getInvoice({commit, state, dispatch}){
    const invoice = this._vm.$functions.httpsCallable('getInvoiceUrl')
    invoice({transaction_id: state.token})
      .then(res => commit('setUrl', res.data.invoice_url || 'error'))
      .catch(e => {
        commit('app_logic/setSnackBar', e.message, {root: true})
        commit('setUrl', 'error')
        dispatch('app_logic/logError', {
          error: e,
          origin: 'invoice/getInvoice',
          misc: state.token,
        }, {root: true})
      })
  }
}

const mutations = {
  setToken(state, payload){
    Vue.set(state, 'token', payload)
  },
  setUrl(state, payload){
    Vue.set(state, 'url', payload)
  }
}

export default {
    state,
    // getters,
    actions, 
    mutations,
    namespaced: true,
}
