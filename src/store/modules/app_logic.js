import Vue from 'vue'

const state = () => ({ 
    snackBar: {
        text: '',
        display: false,
      },
})

const actions = {
  logError({rootState, commit}, payload){
    const logError = this._vm.$functions.httpsCallable('logError')
    logError({
      stack: payload.error.stack,
      origin: payload.origin,
      misc: `User: ${rootState.user.user && rootState.user.user.uid }\n\nMisc: ${payload.misc || ''}`
    })
    commit('setError', payload.error)
  },
  // eslint-disable-next-line no-empty-pattern
  sendSupportMessage({}, payload){
    const supportMessage = this._vm.$functions.httpsCallable('sendSupportMessage')
    supportMessage({
      ...payload
    })
  }
}

const mutations = {
    setSnackBar (state, text) {
        state.snackBar = {display: true, text}
      },
      setLoading (state, payload) {
        Vue.set(state, 'loading', payload)
      },
      setError (state, payload) {
        state.error = payload
      },
      clearError (state) {
        state.error = null
      },
}

export default {
    state,
    // getters,
    actions, 
    mutations,
    namespaced: true,
}
