import Vue from 'vue'
import firebase from 'firebase'
import router from '@/routes'

const initialState = () => {
  
  return {
    user: null,
    userProfile: {
        modified: [],
        first_name: '',
        last_name: '',
        company_name: '',
        email: '',
        phone: '',
        formatted_phone_number: false,
        fortnox_data: {
          api_key: '', // '03829665-a1b9-5e31-df54-7f11616a3998',
          access_token: '', // "be8d488e-7ac3-4a7a-810b-301ec87d71c1",
          articles: [],
        },
        swish_data: {
          payeeAlias: '',
        },
        account_number: '',
        clearing_number: '',
      },
    }
  }

  const state = initialState()

  const getters = {
    user (state) {
        return state.user
      },
      userProfile (state) {
        return state.userProfile
      },
  }

  const actions = {
    async sendPasswordResetMail({commit}, payload){
        const auth = firebase.auth()
        auth.languageCode = 'sv';
        return auth.sendPasswordResetEmail(payload.email).then(() => {
          commit('app_logic/setSnackBar', 'Återställningsmail har skickats', {root: true})
          return 'success'
        }).catch(error => {
          commit('app_logic/setSnackBar', error.message,{root: true})
          return 'error'
        });
      },
      async signUserUpWithOauth ({commit, dispatch}, payload){
        const provider = payload === 'google' ? new firebase.auth.GoogleAuthProvider() : new firebase.auth.FacebookAuthProvider();
        await firebase.auth().signInWithPopup(provider).then(async result => {
          const user = result.user;
          commit('setUser', {
            id: user.uid,
            email: user.email,
            })
          const doc_ref = firebase.firestore().collection("users").doc(user.uid);
          const doc = await doc_ref.get();
          if(doc.exists) {
            commit('setProfile', doc.data())
          } else {
          dispatch('app_logic/sendSupportMessage', {
            message: 'New user onboarded!!! ',
            email: user.email,
          }, {root: true})
          const displayName = user.displayName || ''
          const first_name = displayName.toString().substring(0, displayName.toString().lastIndexOf(' '))
          const last_name = displayName.toString().substring(displayName.toString().lastIndexOf(' ') + 1, displayName.toString().length)
          const newUser = {
            email: user.email,
            created_at: new Date().toISOString(),
            first_name,
            last_name,
            fortnox_data: {
              api_key: process.env.VUE_APP_TESTMODE_FORTNOX_KEY,
              access_token: 'be8d488e-7ac3-4a7a-810b-301ec87d71c1',
              articles: [],
            },
            swish_data: {
              payeeAlias: '1234567890'
            },
          }
          await doc_ref.set(newUser) 
          commit('setProfile', newUser)
          dispatch('updateArticlesFromFortnox')
          }
          return result
        })
        .catch(function(error) {
          commit('app_logic/setSnackBar', error.message, {root: true})
          dispatch('app_logic/logError', {
            error,
            origin: 'signUserUpWithOauth',
          }, {root: true})
        });
        return true
      },
      async signUserUp ({commit, dispatch}, payload) {
        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          async result => {
            const user = result.user;
            commit('setUser', {
              id: user.uid,
              email: payload.email,
              })
            const newUser = {
              email: payload.email,
              created_at: new Date().toISOString(),
              fortnox_data: {
                api_key: process.env.VUE_APP_TESTMODE_FORTNOX_KEY,
                access_token: 'be8d488e-7ac3-4a7a-810b-301ec87d71c1',
                articles: [],
              },
              swish_data: {
                payeeAlias: '1234567890'
              },
            }
            await firebase.firestore().collection("users").doc(user.uid).set(newUser) 
            commit('setProfile', newUser)
            dispatch('updateArticlesFromFortnox')
          }
        )
        .then(() => {
          firebase.auth().currentUser.sendEmailVerification()
          dispatch('app_logic/sendSupportMessage', {
            message: 'New user onboarded!!! ',
            email: payload.email,
          }, {root: true})
          router.push('/dashboard')
        })
        .catch(function(error) {
          commit('app_logic/setSnackBar', error.message, {root: true})
          dispatch('app_logic/logError', {
            error,
            origin: 'signUserUp',
          }, {root: true})
        }) 
      },
      signUserOut ({commit}) {
        commit('resetProfile')
      },
      fetchUserData ({commit, state, dispatch}) {
        // denna får bara köra vid app mount... 
        if(!state.user) return  
        if(!state.user.emailVerified) commit('app_logic/setSnackBar', {text: 'Din mail är inte verifierad.', button: 'Skicka igen', action: () => {firebase.auth().currentUser.sendEmailVerification()}}, {root: true})
        let currentUser = state.user.uid
        commit('app_logic/setLoading', true, {root: true})
        var docRef = firebase.firestore().collection('/users').doc(currentUser)
        docRef.get().then(function(doc) {
          commit('app_logic/setLoading', false, {root: true})
          if (doc.exists) {
            commit('setProfile', doc.data())
          } else {
            commit('resetProfile')
          }
        })
        .catch(
          error => {
            commit('app_logic/setLoading', false, {root: true})
            dispatch('app_logic/logError', {
              error,
              origin: 'fetchUserData',
            }, {root: true})
          }
        )
      },
      async updateUserData ({commit, dispatch}, payload, segment) {
        commit('app_logic/setLoading', true, {root: true})
        firebase.firestore().collection('users').doc(firebase.auth().currentUser.uid)
        .set(payload, { merge: true })
        .then(() => {
          commit('app_logic/setLoading', false, {root: true})
          commit('removeModifiedProfile', segment, {root: true})
          commit('setProfile', payload)
        })
        .catch(error => {
          dispatch('app_logic/logError', {
            error,
            origin: 'updateUserData',
          }, {root: true})
          commit('app_logic/setLoading', false, {root: true})
        })
      },
      async authorizeFortnox({commit, state, dispatch}, payload) {
        commit('app_logic/setLoading', true, {root: true})
        const authorizeFortnox = this._vm.$functions.httpsCallable('authorizeFortnox');
        authorizeFortnox(payload).then(async result => {
          await firebase.firestore().collection('users').doc(firebase.auth().currentUser.uid)
            .set({
              fortnox_data: {
                api_key: payload.fortnox_data.api_key,
                access_token: result.data,
                }
              }, {merge: true}
            ).then(() => {
              state.userProfile.fortnox_data.access_token = result.data;
              commit('app_logic/emoveModifiedProfile', 'fortnox', {root: true})
              commit('app_logic/setLoading', false, {root: true})
            }).catch(error => {
              dispatch('app_logic/logError', {
                error,
                origin: 'authorizeFortnox',
              }, {root: true})
              commit('app_logic/setLoading', false, {root: true})
            })
        }).then(this.dispatch('updateArticlesFromFortnox'));
      },
      async revokeFortnox({commit}, payload) {
        commit('app_logic/setLoading', true, {root: true})
        const revokeFortnox = this._vm.$functions.httpsCallable('revokeFortnox');
        revokeFortnox(payload).then(() => {
          commit('removeModifiedProfile', 'fortnox', {root: true})
          commit('setProfile', {fortnox_data: {} })
          commit('app_logic/setLoading', false, {root: true})
        })
        .catch(() => {
              commit('removeModifiedProfile', 'fortnox', {root: true})
              commit('setProfile', {fortnox_data: {} })
              commit('app_logic/setLoading', false, {root: true})
            })
      },
      async updateArticlesFromFortnox ({commit, state}) {
        commit('app_logic/setLoading', true, {root: true})
        const articles = this._vm.$functions.httpsCallable('getArticlesFromFortnox');
        await articles().then(result => {
          // this.state.userProfile.fortnox_data.articles = result.data;
          let fortnox_data = {...state.userProfile.fortnox_data}
          fortnox_data.articles = result.data
          commit('setProfile', { fortnox_data })
          commit('app_logic/setLoading', false, {root: true})
        })
        return true 
      },
  }

  const mutations = {
    resetProfile (state) {
      Vue.set(state, 'userProfile', initialState().userProfile)
    },
    updateUser (state, { user }) {
        Vue.set(state, 'user', user)
      },
      setUser (state, payload) {
        Vue.set(state, 'user', payload)
      },
      setProfile (state, payload) {
        Object.keys(payload).forEach( key => {
          Vue.set(state.userProfile, key, payload[key])
        })
        
      },
      setModifiedArticles(state){
        const articles = state.userProfile.fortnox_data.articles.map(article => {
          return { ...article, gross_price: (Math.round((article.SalesPrice * (1 + (Number(article.VAT) / 100))) * 100) / 100)}
        });
        Vue.set(state.userProfile.fortnox_data, 'articles', articles)
      },
  }


  export default {
      state,
      getters,
      actions, 
      mutations,
      namespaced: true,
  }