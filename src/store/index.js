import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'

import user from './modules/user'
import transaction from './modules/transaction'
import app_logic from './modules/app_logic'
import invoice from './modules/invoice'

// import createLogger from '../../../src/plugins/logger'

Vue.use(Vuex)

// const debug = process.env.NODE_ENV !== 'production'
const state = {}

const store = new Vuex.Store({
    state,
    modules: {
      app_logic,
      transaction,
      user,
      invoice,
    },
    getters: { getField },
    mutations: { 
      setModifiedProfile: (state, segment) => {
        if(state.user.userProfile.modified.indexOf(segment) === -1) state.user.userProfile.modified.push(segment)
      },
      removeModifiedProfile: (state, segment) => {
        state.user.userProfile.modified.splice(state.user.userProfile.modified.indexOf(segment), 1)
      },
      updateField 
    },
    namespaced: true,
  //   strict: debug,
  //   plugins: debug ? [createLogger()] : []
  })

  store.subscribe((mutation) => {
    if(mutation.type !== 'user/setModifiedArticles' && mutation.payload && mutation.payload.fortnox_data && mutation.payload.fortnox_data.articles){
      store.commit('user/setModifiedArticles', null, {root: true})
    }
    if (mutation.type === "updateField" && (mutation.payload.path === 'user.userProfile.company_name' || mutation.payload.path === 'user.userProfile.first_name' || mutation.payload.path === 'user.userProfile.last_name' || mutation.payload.path === 'user.userProfile.phone')) {
      store.commit("setModifiedProfile", 'profile', {root: true})
    }
    if (mutation.type === "updateField" && (mutation.payload.path.indexOf('fortnox_data') !== -1)) {
      store.commit("setModifiedProfile", 'fortnox', {root: true})
    }
    if (mutation.type === "updateField" && (mutation.payload.path.indexOf('swish_data') !== -1)) {
      store.commit("setModifiedProfile", 'swish', {root: true})
    }
    if (mutation.type === "updateField" && (mutation.payload.path === 'user.userProfile.account_number' || mutation.payload.path === 'user.userProfile.clearing_number')) {
      store.commit("setModifiedProfile", 'bank', {root: true})
    }
    if(mutation.type === "updateField" && (mutation.payload.path.indexOf('fortnox_data.articles') !== -1)){
      store.commit("transaction/resetCart", null, {root: true})
    }
  })

export default store