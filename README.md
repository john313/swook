#swook

##Setup

1. Get and place all .env keys, then
```
cd <PROJECT ROOT FOLDER>
npm install
npm run serve
```

2. If you HAVE installed and setup firebase + emulators (otherwise go to 3)
```
cd functions
npm install

// IF you do have Firebase setup properly globally
firebase emulators:start
```

3. Set up firebase & firebase emulators if you haven't already
```
npm install -g firebase-tools
firebase init emulators
```
Make sure emulators run on:
- functions: 5001
- firestore: 8081

Then do #2 - start emulators

